package com.test.main.controller;

import com.test.model.User;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class UserController {

    @RequestMapping("/api/user")
    public User getUser(){
        User user = new User();
        user.setAddress("Gurgaon");
        user.setUserId(1);user.setUserName("acd");
        return user;
    }
}
